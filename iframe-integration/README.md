## Example for Iframe Integration ##
------------------

You just need to download `index.html` and create your own API Token.

## Create API Token ##
------------------

1. Login [login.bornemann.net](https://login.bornemann.net/portal/account)
2. Switch to tab `Security / Sicherheit`
3. Hit on `Token (rightwards arrow)`
4. Hit on `Plus` to create new API Token.
5. Confirm
6. Hit on `Details` of created Token
7. Copy `value` to clipboard
