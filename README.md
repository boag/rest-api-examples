# Bornemann AG REST API examples #
------------------

This repository shows you how to use the Bornemann REST API. You can find more information in the [Bornemann API Developer Hub](https://developer.bornemann.net/). 




## Examples ##
------------------

* `iframe-integration`: simple integration as iframe